<?php

declare(strict_types=1);

require $_SERVER['DOCUMENT_ROOT'] . '/src/Controller/ResultsController.php';

session_start();

$resultsController = new ResultsController();
$resultsController->resultsAction($_GET, $_SESSION);
