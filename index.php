<?php

declare(strict_types=1);

require $_SERVER['DOCUMENT_ROOT'] . '/src/Controller/StartController.php';
require $_SERVER['DOCUMENT_ROOT'] . '/src/Controller/DatabaseController.php';

session_destroy();
session_start();

$databaseController = new DatabaseController();
$databaseController->deleteData();

$startController = new StartController();
$startController->startAction();
