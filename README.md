![Black Jack](images/logo.png)

## Abstract

Aus einem Leistungsnachweis im zweiten Lehrjahr (Fachinformatiker Anwendungsentwicklung) ist dieses kleine Black-Jack-Spiel entstanden.

[DEMO](http://62.113.211.220/index.php)

Die kleine Web-Anwendung ist in PHP geschrieben und hat folgende Abhängigkeiten.

* [Bulma][1] als CSS-Framework
* [Twig][2] als Template Engine
* [PHPUnit][3] für die Unittests

Als Datenbank zur Speicherung der Spielergebnisse wird die Datei-basierte Datenbank SQLite3 verwendet.

Der Code ist opjektorientiert und konzeptionell ein Versuch, das sogenannten [MVC-Design-Pattern][4] umzusetzen. Dabei werden die Datenverwaltung (Model), das Frontend (View) und die Logik/Steuerung (Controller) voneinander getrennt. Dieser Ansatz ergibt sich aus den von Robert C. Martin geprägten [SOLID-Prinzipien][5], die für sauberen (Clean Code), wiederverwendbaren und wartbaren Code sorgen sollen.

## Spielablauf

1. In einem ersten Schritt müssen die Namen der Spieler eingegeben werden. Üblicherweise ist die Anzahl der Spieler beim Black Jack auf sieben begrenzt. Die Namen dürfen keine Leerzeichen enthalten. Werden keine Namen eingeben, wir automatisch der Spieler JohnDoe generiert.
2. In der nächsten Maske können die Spieler über ein Auswahl-Menü ihre Einsätze tätigen und diese mit einem Klick auf den Button *Bet* bestätigen.
3. Danach werden die Spielkarten aufgedeckt und der aktuelle Wert der Blätter angezeigt. Die Spieler haben nun die Möglichkeit, eine zusätzliche Karte zu zu bekommen (*Hit*) oder ihr Blatt auf dem aktuellen Stand zu belassen (*Stand*). Der Dealer bekommt eine neue Karte, wenn der aktuelle Wert seines Blattes unter 17 liegt. Der Button *Action* bestätigt die entsprechenden Entscheidungen.
4. Im letzten Schritt werden die Ergebnisse der aktuellen Runde sowie der vorherigen Runden (Tabelle) angezeigt. Die Spieler haben jetzt die Möglichkeit, eine neue Runde (*New Round*) oder ein neues Spiel (*New Game*) zu beginnen.

## Klassenstruktur

Die Logik des Spiels wird im Wesentlichen durch folgende Klassen und deren Methoden realisiert.

**Card** definiert eine Spielkarte und deren Eigenschaften wie Farbe, Wert, das Bild der Vorder- und Rückseite, etc.

* Card
    - getSuit(): string
    - getSymbol(): string
    - getValue(): int
    - getImage(): string
    - getBackside(): string
    - isPlayed(): bool
    - setValue(): void
    - setIsPlayed(): void

Das **Deck** beinhaltet sechs übliche Karten-Pakete je 52 Karten, umfasst also einen Kartenvorrat von 312 Karten. Es können einzelne oder zwei zufällige Karten (Hand) ausgegeben werden. Nachdem eine Karte ausgegeben wurde, wird sie als gespielt markiert und kann im Folgenden nicht noch mal ausgegeben werden.

* Deck
    - takeRandomCard(): Card
    - generateHand(): Hand

**Hand** fasst alle Karten zusammen, die ein Spieler (Player) oder der Dealer in der Hand hält und berechnet die Summe der entsprechenden Kartenwerte.

* Hand
    - getCards(): array
    - getValue(): int
    - addCard(Card card): void

Der **Dealer** und die **Player** sind die Teilnehmer des Spiels und interagieren mit- bzw. gegeneinander.

* Dealer
    - getHand(): Hand
    - setHand(Hand hand): void
* Player
    - getName(): string
    - getWin(): float
    - getHand(): Hand
    - getBet(): int
    - setBet(int bet): void
    - setWin(float win): void
    - set Hand(Hand hand): void

Der **WinCalculator** berechnet den Gewinn eines Spielers pro Runde in Abhängigkeit des Zustandes des Dealers. Er berücksichtigt z.B. einen Black Jack (21 Punkte in einem Blatt) mit der 1.5-fachen Auszahlung des Einsatzes.

* WinCalculator
    - calculateWin(Dealer dealer, Player player): float

Die **Controller** sind zuständig für das Rendern der einzelnen Views. Ihre Logik steuert die Interaktion der einzelnen Objekte und übergibt die Ergebnisse an die jeweiligen Twig-Templates im Ordner *views*. Bis auf den **DatabaseController** erben alle Controller von der abstrakten Klasse **ViewController**.

* ActionController
    - actionAction(array get, array session): void
* BetController
    - betAction(array get): void
* DatabaseController
    - writeData(array players): void
    - readData(array players): array
    - deleteData(): void
* NewGameController
    - newGameAction(array session): void
* ResultController
    - resultAction(array get, array session)
* StartController
    - startAction(): void
* ViewController *(abstract)*
    - displayWithErrorCatch(string template, array parameters): void

[1]: https://bulma.io
[2]: https://twig.symfony.com
[3]: https://phpunit.de
[4]: https://de.wikipedia.org/wiki/Model_View_Controller
[5]: https://de.wikipedia.org/wiki/Prinzipien_objektorientierten_Designs
