<?php

declare(strict_types=1);

require $_SERVER['DOCUMENT_ROOT'] . '/src/Controller/BetController.php';

session_start();

$betController = new BetController();
$betController->betAction($_GET);
