<?php

declare(strict_types=1);

class Card
{
    /**
     * @var string
     */
    private $suit;

    /**
     * @var string
     */
    private $symbol;

    /**
     * @var int
     */
    private $value;

    /**
     * @var string
     */
    private $image;

    /**
     * @var string
     */
    private $backside;

    /**
     * @var bool
     */
    private $isPlayed;

    public function __construct(
        string $suit,
        string $symbol
    ){
        $this->suit = $suit;
        $this->symbol = $symbol;
        $this->isPlayed = false;
        $this->backside = '../images/cards/backside.png';
        $this->image = '../images/cards/';

        switch ($this->suit) {
            case 'clubs':
                $this->image .= 'clubs/';
                break;
            case 'diamonds':
                $this->image .= 'diamonds/';
                break;
            case 'hearts':
                $this->image .= 'hearts/';
                break;
            case 'spades':
                $this->image .= 'spades/';
                break;
        }

        switch ($this->symbol) {
            case '2':
                $this->value = 2;
                $this->image .= '02.png';
                break;
            case '3':
                $this->value = 3;
                $this->image .= '03.png';
                break;
            case '4':
                $this->value = 4;
                $this->image .= '04.png';
                break;
            case '5':
                $this->value = 5;
                $this->image .= '05.png';
                break;
            case '6':
                $this->value = 6;
                $this->image .= '06.png';
                break;
            case '7':
                $this->value = 7;
                $this->image .= '07.png';
                break;
            case '8':
                $this->value = 8;
                $this->image .= '08.png';
                break;
            case '9':
                $this->value = 9;
                $this->image .= '09.png';
                break;
            case '10':
                $this->value = 10;
                $this->image .= '10.png';
                break;
            case 'jack':
                $this->value = 10;
                $this->image .= 'jack.png';
                break;
            case 'queen':
                $this->value = 10;
                $this->image .= 'queen.png';
                break;
            case 'king':
                $this->value = 10;
                $this->image .= 'king.png';
                break;
            case 'ace':
                $this->value = 11;
                $this->image .= 'ace.png';
                break;
        }
    }

    public function getSuit(): string
    {
        return $this->suit;
    }

    public function getSymbol(): string
    {
        return $this->symbol;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function getBackside(): string
    {
        return $this->backside;
    }

    public function isPlayed(): bool
    {
        return $this->isPlayed;
    }

    public function setValue(int $value): void
    {
        $this->value = $value;
    }

    public function setIsPlayed(): void
    {
        $this->isPlayed = true;
    }
}
