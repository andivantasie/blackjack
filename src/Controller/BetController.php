<?php

declare(strict_types=1);

require 'ViewController.php';

class BetController extends ViewController
{
    public function betAction(array $get): void
    {
        $deck = new Deck();
        $dealer = new Dealer($deck->generateHand());
        $players = [];
        $numberEmptyNames = 0;

        foreach ($get as $playerName) {
            if (!empty($playerName)) {
                $players[] = new Player($playerName, $deck->generateHand());
            } else {
                $numberEmptyNames++;
            }
        }

        if ($numberEmptyNames === 7) {
            $players[] = new Player('JohnDoe', $deck->generateHand());
        }

        $_SESSION['deck'] = $deck;
        $_SESSION['dealer'] = $dealer;
        $_SESSION['players'] = $players;

        $this->displayWithErrorCatch('bet.html.twig', [
            'bets' => self::BETS,
            'dealer' => $dealer,
            'players' => $players
        ]);
    }
}
