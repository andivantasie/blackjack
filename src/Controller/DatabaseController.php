<?php

class DatabaseController
{
    public function __construct()
    {
        try {
            $this->db = new PDO('sqlite:data.db');
        } catch (PDOException $e) {
            $e->getMessage();
        }
    }

    public function writeData(array $players): void
    {
        $sql = 'CREATE TABLE IF NOT EXISTS game_results (round INTEGER PRIMARY KEY, ';
        foreach ($players as $player) {
            $sql .= 'value_' . $player->getName() . ' INTEGER, ';
            $sql .= 'bet_' . $player->getName() . ' INTEGER, ';
            $sql .= 'win_' . $player->getName() . ' REAL, ';
        }
        $sql = substr($sql, 0, -2);
        $sql .= ');';

        $this->db->exec($sql);

        $sql = 'INSERT INTO game_results (';
        foreach ($players as $player) {
            $name = $player->getName();
            $sql .= 'value_' . $name . ', bet_' . $name . ', win_' . $name . ', ';
        }
        $sql = substr($sql, 0, -2);
        $sql .= ') VALUES (';
        foreach ($players as $player) {
            $sql .= $player->getHand()->getValue() . ', ' . $player->getBet() . ', ' . $player->getWin() . ', ';
        }
        $sql = substr($sql, 0, -2);
        $sql .= ');';

        $this->db->exec($sql);
    }

    public function readData(array $players): array
    {
        $sql = "SELECT * FROM game_results";
        $results = $this->db->query($sql);
        $data = [];
        foreach ($results as $result) {
            $row = [];
            $row[] = $result['round'];
            foreach ($players as $player) {
                $row[] = $result['value_' . $player->getName()];
                $row[] = $result['bet_' . $player->getName()];
                $row[] = $result['win_' . $player->getName()];
            }
            $data[] = $row;
        }
        return $data;
    }

    public function deleteData(): void
    {
        $sql = 'DROP TABLE game_results';
        $this->db->exec($sql);
    }
}