<?php

declare(strict_types=1);

require 'ViewController.php';
require 'DatabaseController.php';
require $_SERVER['DOCUMENT_ROOT'] . '/src/WinCalculator.php';

class ResultsController extends ViewController
{
    public function resultsAction(array $get, array $session): void
    {
        $deck = $session['deck'];
        $dealer = $session['dealer'];
        $players = $session['players'];

        if ($dealer->getHand()->getValue() <= 17) {
            $dealer->getHand()->addCard($deck->takeRandomCard());
        }

        $winCalculator = new WinCalculator();

        foreach ($players as $player) {
            $key = 'isHitStand' . $player->getName();
            if ($get[$key] === 'hit') {
                $player->getHand()->addCard($deck->takeRandomCard());
            }
            $win = $winCalculator->calculateWin($dealer, $player);
            $player->setWin($win);
        }

        $_SESSION['deck'] = $deck;
        $_SESSION['dealer'] = $dealer;
        $_SESSION['players'] = $players;

        $databaseController = new DatabaseController();
        $databaseController->writeData($players);
        $data = $databaseController->readData($players);

        $this->displayWithErrorCatch('results.html.twig', [
            'players' => $players,
            'dealer' => $dealer,
            'data' => $data
        ]);
    }
}
