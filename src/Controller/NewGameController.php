<?php

declare(strict_types=1);

require 'ViewController.php';

class NewGameController extends ViewController
{
    public function newGameAction(array $session): void
    {
        $deck = $_SESSION['deck'];
        $dealer = $_SESSION['dealer'];
        $players = $_SESSION['players'];

        $dealer->setHand($deck->generateHand());

        foreach ($players as $player) {
            $player->setHand($deck->generateHand());
        }

        $_SESSION['deck'] = $deck;
        $_SESSION['dealer'] = $dealer;
        $_SESSION['players'] = $players;

        $this->displayWithErrorCatch('bet.html.twig', [
            'bets' => self::BETS,
            'dealer' => $dealer,
            'players' => $players
        ]);
    }
}
