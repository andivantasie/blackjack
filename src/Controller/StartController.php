<?php

declare(strict_types=1);

require 'ViewController.php';

class StartController extends ViewController
{
    public function startAction(): void
    {
        $this->displayWithErrorCatch('start.html.twig');
    }
}
