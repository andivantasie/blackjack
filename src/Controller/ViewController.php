<?php

declare(strict_types=1);

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;

require $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
require $_SERVER['DOCUMENT_ROOT'] . '/src/Dealer.php';
require $_SERVER['DOCUMENT_ROOT'] . '/src/Deck.php';
require $_SERVER['DOCUMENT_ROOT'] . '/src/Player.php';

abstract class ViewController
{
    protected const BETS = [2, 5, 10, 20, 50, 100, 500, 1000, 5000, 10000];

    /**
     * @var string
     */
    private $templateDir;

    public function __construct()
    {
        $this->templateDir = $_SERVER['DOCUMENT_ROOT'] . '/views/';
        $this->loader = new FilesystemLoader($this->templateDir);
        $this->twig = new Environment($this->loader);
    }

    protected function displayWithErrorCatch(string $template, array $parameters = []): void
    {
        try {
            $this->twig->display($template, $parameters);
        } catch (LoaderError $e) {
            echo $e->getMessage();
        } catch (RuntimeError $e) {
            echo $e->getMessage();
        } catch (SyntaxError $e) {
            echo $e->getMessage();
        }
    }
}
