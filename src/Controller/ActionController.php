<?php

declare(strict_types=1);

require 'ViewController.php';
require $_SERVER['DOCUMENT_ROOT'] . '/src/WinCalculator.php';

class ActionController extends ViewController
{
    public function actionAction(array $get, array $session): void
    {
        $dealer = $session['dealer'];
        $players = $session['players'];

        $winCalculator = new WinCalculator();

        foreach ($players as $player) {
            $key = 'bet' . $player->getName();
            $player->setBet((int)$get[$key]);
            $win = $winCalculator->calculateWin($dealer, $player);
            $player->setWin($win);
        }

        $_SESSION['players'] = $players;

        $this->displayWithErrorCatch('action.html.twig', [
            'players' => $players,
            'dealer' => $dealer
        ]);
    }
}
