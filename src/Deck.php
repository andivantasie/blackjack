<?php

declare(strict_types=1);

require 'Card.php';
require 'Hand.php';

class Deck
{
    private const NUMBER_PACKAGES = 6;
    private const SUITS = ['clubs', 'diamonds', 'hearts', 'spades'];
    private const SYMBOLS = ['ace', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'jack', 'queen', 'king'];

    public function __construct()
    {
        $this->cards = [];

        for ($i = 0; $i < self::NUMBER_PACKAGES; $i++) {
            foreach (self::SUITS as $suit) {
                foreach (self::SYMBOLS as $symbols) {
                    $card = new Card($suit, $symbols);
                    $this->cards[] = $card;
                }
            }
        }
    }

    public function takeRandomCard(): Card
    {
        $cards = [];
        foreach ($this->cards as $card) {
            if ($card->isPlayed() === false) {
                $cards[] = $card;
            }
        }
        shuffle($cards);
        $card = $cards[0];
        $card->setIsPlayed();
        return $card;
    }

    public function generateHand(): Hand
    {
        $cards = [];
        for ($i = 0; $i < 2; $i++) {
            $cards[] = $this->takeRandomCard();
        }
        return new Hand($cards);
    }
}
