<?php

declare(strict_types=1);

class Player
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var Hand
     */
    private $hand;

    /**
     * @var int
     */
    private $bet;

    /**
     * @var float
     */
    private $win;

    public function __construct(string $name, Hand $hand)
    {
        $this->name = $name;
        $this->hand = $hand;
        $this->bet = 0;
        $this->win = 0.0;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getWin(): float
    {
        return $this->win;
    }

    public function getHand(): Hand
    {
        return $this->hand;
    }

    public function getBet(): int
    {
        return $this->bet;
    }

    public function setBet(int $bet): void
    {
        $this->bet = $bet;
    }

    public function setWin(float $win): void
    {
        $this->win = $win;
    }

    public function setHand(Hand $hand): void
    {
        $this->hand = $hand;
    }
}
