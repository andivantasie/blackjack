<?php

declare(strict_types=1);

class Dealer
{
    /**
     * @var Hand
     */
    private $hand;

    public function __construct(Hand $hand)
    {
        $this->hand = $hand;
    }

    public function getHand(): Hand
    {
        return $this->hand;
    }

    public function setHand(Hand $hand): void
    {
        $this->hand = $hand;
    }
}
