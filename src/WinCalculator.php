<?php

declare(strict_types=1);

class WinCalculator
{
    public function calculateWin(Dealer $dealer, Player $player): float
    {
        $valueDealer = $dealer->getHand()->getValue();
        $valuePlayer = $player->getHand()->getValue();

        $diffTo21Dealer = abs(21 - $valueDealer);
        $diffTo21Player = abs(21 - $valuePlayer);

        if ($valuePlayer === 21) {
            $win = 1.5 * $player->getBet();
            if ($valueDealer === 21) {
                $win = 0;
            }
        } else {
            if ($valuePlayer > 21) {
                $win = -$player->getBet();
            } elseif ($valuePlayer === $valueDealer) {
                $win = 0;
            } else {
                if ($valueDealer < 21) {
                    if ($diffTo21Player > $diffTo21Dealer) {
                        $win = -$player->getBet();
                    } elseif ($diffTo21Player < $diffTo21Dealer) {
                        $win = $player->getBet();
                    }
                } else {
                    $win = $player->getBet();
                }
            }
        }

        return (float)$win;
    }
}