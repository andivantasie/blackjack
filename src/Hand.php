<?php

declare(strict_types=1);

class Hand
{
    /**
     * @var array
     */
    private $cards;

    /**
     * @var int
     */
    private $value;

    public function __construct(array $cards)
    {
        $this->cards = $cards;
        $this->value = $this->calcValue();
    }

    public function getCards(): array
    {
        return $this->cards;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function addCard(Card $card): void
    {
        if ($card->getSymbol() === 'ace' && (21 - $this->value) <= 11) {
            $card->setValue(1);
        }
        $this->cards[] = $card;
        $this->value += $card->getValue();
    }

    public function calcValue(): int
    {
        if ($this->cards[0]->getSymbol() === 'ace' && $this->cards[1]->getSymbol() === 'ace') {
            $this->cards[1]->setValue(1);
        }
        $value = 0;
        foreach ($this->cards as $card) {
            $value += $card->getValue();
        }
        return $value;
    }
}
