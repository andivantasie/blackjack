<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

require '../src/Card.php';
require '../src/Hand.php';

class HandTest extends TestCase
{
    /**
     * @dataProvider calcValueProvider
     */
    public function testCalcValue($hand, $pointsExpected): void
    {
        $this->assertEquals($pointsExpected, $hand->calcValue());
    }

    public function calcValueProvider(): array
    {
        $card1 = new Card('hearts', '2');
        $card2 = new Card('clubs', '10');
        $card3 = new Card('diamonds', 'ace');
        $card4 = new Card('hearts', 'ace');

        $hand1 = new Hand([$card1, $card2]);
        $hand2 = new Hand([$card2, $card3]);
        $hand3 = new Hand([$card1, $card3]);
        $hand4 = new Hand([$card3, $card4]);

        return [
            'hearts 2 and clubs 10'       => [$hand1, 12],
            'black jack'                  => [$hand2, 21],
            'hearts 2 and diamond ace'    => [$hand3, 13],
            'diamonds ace and hearts ace' => [$hand4, 12] // value of 2nd ace changes to 1
        ];
    }

    /**
     * @dataProvider addCardProvider
     */
    public function testAddCard($card, $hand, $handExpected): void
    {
        $hand->addCard($card);
        $this->assertEquals($handExpected, $hand);
    }

    public function addCardProvider(): array
    {
        $card1 = new Card('hearts', '2');
        $card2 = new Card('clubs', '10');
        $card3 = new Card('clubs', '8');

        $hand1 = new Hand([$card1, $card2]);
        $hand2 = new Hand([$card1, $card2, $card3]);

        return [
            'add clubs 8' => [$card3, $hand1, $hand2]
        ];
    }
}
