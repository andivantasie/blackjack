<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

require '../src/Card.php';
require '../src/Dealer.php';
require '../src/Hand.php';
require '../src/Player.php';
require '../src/WinCalculator.php';

class WinCalculatorTest extends TestCase
{
    /**
     * @dataProvider calculateWinProvider
     */
    public function testCalculateWin($dealer, $player, $winExpected): void
    {
        $winCalculator = new WinCalculator();
        $win = $winCalculator->calculateWin($dealer, $player);
        $this->assertEquals($winExpected, $win);
    }

    public function calculateWinProvider(): array
    {
        $symbols = ['ace', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'jack', 'queen', 'king'];

        foreach ($symbols as $symbol) {
            $card[$symbol] = new Card('hearts', $symbol);
        }

        $hand1 = new Hand([$card['2'], $card['5']]); // value 7
        $hand2 = new Hand([$card['5'], $card['7']]); // value 13
        $hand3 = new Hand([$card['ace'], $card['jack']]); // value 21, black jack
        $hand4 = new Hand([$card['5'], $card['jack'], $card['10']]); // value 25, busted

        $dealer1 = new Dealer($hand1);
        $player1 = new Player('Otto', $hand2);
        $player1->setBet(100);

        $dealer2 = new Dealer($hand2);
        $player2 = new Player('Otto', $hand1);
        $player2->setBet(100);

        $dealer3 = new Dealer($hand1);
        $player3 = new Player('Otto', $hand1);
        $player3->setBet(100);

        $dealer4 = new Dealer($hand2);
        $player4 = new Player('Otto', $hand3);
        $player4->setBet(1000);

        $dealer5 = new Dealer($hand2);
        $player5 = new Player('Otto', $hand4);
        $player5->setBet(10);

        $dealer6 = new Dealer($hand4);
        $player6 = new Player('Otto', $hand2);
        $player6->setBet(10);

        return [
            'player wins'   => [$dealer1, $player1, '100'],
            'player loose'  => [$dealer2, $player2, '-100'],
            'no win'        => [$dealer3, $player3, '0'],
            'black jack'    => [$dealer4, $player4, '1500'],
            'player busted' => [$dealer5, $player5, '-10'],
            'dealer busted' => [$dealer6, $player6, '10']
        ];
    }
}
