<?php

declare(strict_types=1);

require $_SERVER['DOCUMENT_ROOT'] . '/src/Controller/NewGameController.php';

session_start();

$newGameController = new NewGameController();
$newGameController->newGameAction($_SESSION);
