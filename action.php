<?php

declare(strict_types=1);

require $_SERVER['DOCUMENT_ROOT'] . '/src/Controller/ActionController.php';

session_start();

$actionController = new ActionController();
$actionController->actionAction($_GET, $_SESSION);
